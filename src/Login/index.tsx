import { useState } from "react";
import { LoginApi } from "../Api/Login";
import styles from "./styles.module.css";

const Login = () => {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const handleEmailChange = (e: any) => {
        setEmail(e.target.value);
    }

    const handlePasswordChange = (e: any) => {
        setPassword(e.target.value);
    }

    const handleLogIn = () => {
        LoginApi.login(email, password);
    }

    return (
        <div className={styles.mainContainer}>
            <div className={styles.form}>
                <div className={styles.row}>
                    <div className={styles.label}>Email</div>
                    <input type={"text"} onChange={handleEmailChange} value={email}></input>
                </div>
                <div className={styles.row}>
                    <div className={styles.label}>Password</div>
                    <input type={"text"} onChange={handlePasswordChange} value={password}></input>
                </div>
                <button className={styles.button} onClick={handleLogIn}>Log in</button>
            </div>
        </div>
    );
}

export default Login;