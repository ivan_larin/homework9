import axios from "axios";

export class LoginApi {
    static login(email: string, password: string) {
        return axios.post('/api/login', {email, password});
    }
}